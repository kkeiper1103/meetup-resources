// Copyright Kyle Keiper 2012
// License GNU GPL v3
// Feel free to re-use this file, just leave my copyright at the top.

// AXR is a standalone ajax wrapper, designed to feel similar to the jQuery.ajax function, except without the whole jQuery layer.

var AXR = function( options ){
	
	var AXRhndl = this;
	
	this.req = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	this.data = {};
	this.method = "get";
	this.url = null;
	this.async = true;
	this.reqString = "";
	this.returnMessage = "";
	
	//Functions 
	
	this.success = (function( msg ){ var d = document.createElement("div"); d.innerHTML = msg; document.body.appendChild(d); });
	this.error = (function( msg, readyState, status ){ 
		
		var div = document.createElement("div");
		var msgParagraph = document.createElement("div");
		var readyStateParagraph = document.createElement("p");
		var statusParagraph = document.createElement("p");
		
		msgParagraph.innerHTML = msg;
		readyStateParagraph.innerText = "ReadyState: " + readyState;
		statusParagraph.innerText = "Status Code: " + status;
		
		div.appendChild( statusParagraph );
		div.appendChild( readyStateParagraph );
		div.appendChild( msgParagraph );
		
		document.body.appendChild( div );
		
	});
	this.complete = (function( msg ){ var d = document.createElement("div"); d.innerHTML = msg; document.body.appendChild(d); });
	
	this.send = (function(){
		
		if( this.method.toUpperCase() == "GET" ){
			
			this.req.open( this.method, this.url + "?" + this.reqString, this.async );
			this.req.send();
			
		} else {
		
			this.req.open( this.method, this.url, this.async );
			this.req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			this.req.send( this.reqString );
		
		}
		
	});
	
	this.post = (function(){
	
		this.req.open( "POST" , this.url, this.async );
		this.req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		this.req.send( this.reqString );
	
	});
	
	this.get = (function(){
		
		this.req.open( "GET" , this.url + "?" + this.reqString, this.async );
		this.req.send();
		
	});
	
	this.readyStateChange = (function(){
		
		if( this.readyState == 4  ){
			if( this.status == 200 ) {
				AXRhndl.success( this.responseText );
			} else {
				AXRhndl.error( this.responseText, this.readyState, this.status );
			}
		}
		
	});
	
	//Parse the options JSON given in constructor
	for( option in options ){
		
		this[option] = options[option];
		
	}
	
	//Create request string from the data object
	for( datum in this.data ){
		
		this.reqString += datum + "=" + this.data[datum] + "&";
		
	}
	// remove last ampersand from request string.
	
	this.reqString = this.reqString.substr( (0), (this.reqString.length - 1) );
	
	this.method = this.method.toUpperCase();
	
	if( this.url == null )
		return false; //No URL, stop going.
	
	this.req.onreadystatechange = this.readyStateChange;
	
}
