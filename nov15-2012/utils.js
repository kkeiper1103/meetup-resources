var _g/*get*/ = function(el){return document.querySelector(el)}
var _a/*all*/ = function(el){return document.querySelectorAll(el)}
var _c/*create*/ = function(el){return document.createElement(el)}
var _l/*log*/ = function(msg){(window.console) ? console.log(msg) : '';}
var _e/*event*/ = function(el,evt,fn){
	
	if(el.addEventListener){
		//W3C Dom
		el.addEventListener(evt, fn, false);
		
	} else if(el.attachEvent) {
		// Microsoft DOM
		
		
		el.attachEvent("on"+evt, fn);
		
	} else {
		// All other DOM
		el['on'+evt] = fn;
		
		if( el["on"+evt] == null ){
			
			alert("Please Upgrade Your Browser To View Image Gallery.");
			return;
		
		}
		
	}
	
}