document.addEventListener("DOMContentLoaded",(function(e){
			
			var inputs = document.getElementsByClassName( "addHandler" );
			var c = function( el ){
				return document.createElement( el );
			}
			
			var focusInHandler = function( evt ){
			
				var p = c("p");
				p.innerText = this.id;
				p.className = "toolTip";
				this.parentElement.appendChild( p );
			
			}
			
			var focusOutHandler = function( evt ){
			
				var parent = this.parentElement;
				
				parent.removeChild( parent.lastChild );
				
			}
			
			for( var index = 0; index < inputs.length; index++ ){
				
				addEvt( inputs[index], "focus", focusInHandler );
				addEvt( inputs[index], "blur", focusOutHandler );
				
			}
			
		}), false);
