var get = function( el ){
			
			return document.querySelector( el );
			
		}
		
		var addEvt = function( el, evt, fn ){
			
			if( el.addEventListener ) {
				
				el.addEventListener(evt, fn, false);
				
			} else if( el.attachEvent ) {
				
				el.attachEvent( "on" + evt, fn );
				
			} else {
				alert("This Won't Work. Upgrade to Chrome. Now.");
			}
			
		}
		
		document.addEventListener("DOMContentLoaded", (function(){
			
			
			
			//Change Event Handler Resources
			
			var range = get("#range");
		
			addEvt( range, "change", (function(e){
				
				get("#range_display").innerText = "The Current Value Is: " + this.value;
				
			}) );
			
			
			
			//Submit Event Handler Resources
			
			addEvt( get("#theForm"), "submit", (function(e){
				
				e.preventDefault();
				
				get("#submit_display").innerText = "Your Name Is " + get("#fname").value + " " + get("#lname").value + ", and your email is " + get("#email").value;
				
			}));
			
			
			
			//Keyboard Event Handler Resources
			
			var keyBoardHandler = function(e){
				
				e.preventDefault();
				
				var isPressed = (e.type == "keypress" || e.type == "keydown") ? true : false;
				
				var evtObj = {
					
					type : e.type,
					isPressed : isPressed,
					altKey : e.altKey,
					ctrlKey : e.ctrlKey,
					shiftKey : e.shiftKey,
					key : e.keyCode || e.which,
					keyChar : String.fromCharCode( e.keyCode || e.which )
					
				};
				
				get("#key_display_pre").innerText = JSON.stringify( evtObj );
				
			};
			
			
			addEvt( get("#elKeyEvt"), "keydown", keyBoardHandler );
			addEvt( get("#elKeyEvt"), "keyup", keyBoardHandler );
			addEvt( get("#elKeyEvt"), "keypress", keyBoardHandler );
			
		}) );
