var get = function( el ){
	
	var patt = /^((\.)||(\#))/gi;
	
	if( patt.test( el ) ){
		
		var ret = document.querySelectorAll( el );
		return (ret.length > 1) ? ret : ret[0];
		
	} else {
	
		var ret = document.getElementsByTagName( el )
		return (ret.length > 1) ? ret : ret[0];
		
	}
	
	
			
}
		
var addEvt = function( el, evt, fn ){
	
	
	
	if( el.addEventListener ) {
				
		el.addEventListener(evt, fn, false);
				
	} else if( el.attachEvent ) {
				
		el.attachEvent( "on" + evt, fn );
				
	} else {
	
		el[ "on" + evt ] = fn;
		
		if( el[ "on" + evt] == "undefined" )
			alert("This Won't Work. Upgrade to Chrome. Now.");
		
	}
	
			
}

var calcSettings = {
	"lastOp" 		: 		"",
	"clearFlag" 	: 		false,
	"numberInMemory":		0
}

document.addEventListener( "DOMContentLoaded", (function(evt){
	
	
	
	var btns = get(".btn");
	
	var btn_click_handler = function( evt ){
	
		if( calcSettings.clearFlag ){
		
		
			get("#display").value = "";
			
			get("#op_display").innerText = "";
		
		
			calcSettings.lastOp = '';
			calcSettings.numberInMemory = 0;
			calcSettings.clearFlag = false;
		}
		
		switch( this.dataset.btntype ){ // attributes in html are case-insensitive! btnType becomes btntype!
			case "num":
				get( "#display" ).value += this.value;
			break;
			case "util":
				
				calcSettings.numberInMemory = get("#display").value;
				get( "#display" ).value = "";
				
				
				calcSettings.lastOp = this.value;
				get("#op_display").innerText = calcSettings.lastOp;
				
			break;
			case "equals":
					
				var numOnly = /[0-9\-*\/\+\.]/g;
				
				var isDisplayValid = numOnly.test( get("#display").value ); // is the value of the display only numbers and operators?
				
				if( isDisplayValid ){
					
					opString = calcSettings.numberInMemory + calcSettings.lastOp + get("#display").value;
					
					get("#display").value = eval( opString ); // if so, then evaluate it. It's passed inspection
					
				} else {
					
					alert("Possible Code Injection Detected. Please Contact the Webmaster for More Information. Query is Aborted.");
					
					get("#display").value = '0'; // if not, it's possibly been hacked, so prevent evaluation of it.
					
				}
					
				
			break;
			case "clear":
				
				get("#display").value = "";
				
				calcSettings.clearFlag = true;
				
			break;
		}
		
	}
	
	
	for( var btn in btns){
		
		addEvt( btns[btn], "click", btn_click_handler );
		
	}
	
	// Just to stop form submission should it happen by pressing enter...
	
	addEvt( get("#calc"), "submit", (function(e){
		
		e.preventDefault();
		
	}) );
	
}), false );
