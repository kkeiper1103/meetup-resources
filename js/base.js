var get = function( el ){
			
	var patt = /^((\.)||(\#))/gi;
	
	if( patt.test( el ) ){
		
		var ret = document.querySelectorAll( el );
		return (ret.length > 1) ? ret : ret[0];
		
	} else {
	
		var ret = document.getElementsByTagName( el )
		return (ret.length > 1) ? ret : ret[0];
		
	}
			
}

var addEvt = function( el, evt, fn ){
	
	if( el.addEventListener ) {
		
		el.addEventListener(evt, fn, false);
		
	} else if( el.attachEvent ) {
		
		el.attachEvent( "on" + evt, fn );
		
	} else {
		alert("This Won't Work. Upgrade to Chrome. Now.");
	}
	
}
